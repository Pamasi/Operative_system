#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

typedef struct{
	int *vet,n;
}file_t;


void *routine(void *args){
	int tmpi, offset;
	file_t part1, part2;
	pthread_t tid1, tid2;
	file_t *in=(file_t*)args;

	if(in->n==1)
		pthread_exit(NULL);

	offset =  (n/2)%2==0?0:1; 
	part1.vet=in->vet;
	part2.vet=in->vet+n/2; 
	part1.n = in->n/2;
	part2.n = in->n/2 + offset; 

	pthread_create(&tid1, NULL, routine, (void*)&part1);
	pthread_create(&tid2, NULL, routine, (void*)&part2);

	pthread_join(tid1, NULL); pthread_join(tid2, NULL);

	// sorting
	for(int i=0; i<n/2; ++i){
		if( part1.vet[i]>part2.vet[i]){
			tmp=part1.vet[i];
			part1.vet[i]=part2.vet[i];
			part2.vet[i]=tmp;
		}
			
	}
 	if(offset && part2.vet[n/2]<part2.vet[offset]){
		tmp=part2.vet[n/2];
		part2.vet[n/2]=part2.vet[offset];
		part2.vet[offset]=part2.vet[n/2];
		
	}	
	
	pthread_exit(NULL);
}





int main(int argc, char **argv){
	int *vet, n;
	pthread_t tid;
	file_t arg;

	scanf("%d", &n);
	vet = (int*) malloc(n*sizeof(int));
	arg.vet=vet;
	arg.n=n;
	for(int i=0; i<n; scanf("%d", vet[i++]));
	
	pthread_create(&tid, NULL, routine, (void*)&arg);
	pthread_join(tid, NULL)	;

	free(vet);
	return 0;
}
