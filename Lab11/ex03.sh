#!/bin/bash

if [ $# -ne 2 ]; then
	exit 1
fi

res=$(find /home -user $1 -regextype posix-extended -regex "(.+/)*$2(/.+)*" \
	! -type d -exec grep -E "\*\*\*Da modificare.*" -l \{} \;)

if [ -z "$res" ]; then
	echo -n "No file found\n"
	exit 2
fi
# up to this point works

for file in $res; do
	printf "file is=%s\n" $file
	out=${file}"_mod"
	touch $out	
	while read line; do
		remove=$(echo $line | grep -E ".*\*\*\*Da modificare.*")
		if [ -z "$remove" ]; then
			echo $line>>$out
		fi

	done <$file

done
