#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>
#include<sys/wait.h>
#include<sys/types.h>


#define DEBUG 0

// this is reprensent also in child PCB
// in this program it is used only in parent PCB
// 'cause child kill signal to parent 
static void parent(int sig){
#if DEBUG ==1
    if(!getppid())
        fprintf(stdout, "Child caught SIGCHLD\n");
    else
        fprintf(stdout, "Parent caught SIGCHLD\n");
#endif
    sleep(1);
}

int main(int argc, char **argv){
    int pid;


    /*  structure of program
        hadling signals*/

    /*  1) CALL THE SIGNAL HANDLER 
            WITH RELATIVE SIGNAL TO CATCH
            AND THE HANDLING FUNCTION*/

   if(signal(SIGCHLD, parent) ==SIG_ERR)
   {
       fprintf(stderr, "Signal parent handler error\n");
       exit(EXIT_FAILURE);
   }



    /* 2) CORE OF THE PROGRAM*/

    // clear buffer for debugging
    setbuf(stdout, 0);

    pid = fork();
    if(pid==-1){
        
        exit(EXIT_FAILURE);
    }
    /*  SIGNAL HANDLING
        Remember that after fork  you create 2 
        different process that run the same below
        code with different PCB
        so the signal are the same  but operate for
        each process asynchrounsly by default, 
        they DO NOT KNOW ANYTHING ABOUT EACH OTHER
    */
    
    while(1){
        if(!pid){
            printf("Child Woke-up\n");

            // send a signal to its parent
            // that means the parent process 
            // will exec the signal handler(see above declaration)
            // present in ITS PCB 
#if DEBUG==1
           fprintf(stdout, "Child sent signal to Parent\n");
#endif
            kill(getppid(), SIGCHLD); 
            // after the child process send the signal
            // sleep in order to let the parent works
            sleep(3);
        }
        else{
            /*  to synchronize process
                we use sleep: 
                so that child will run fst
                YOU MUST CALCULATE THE SLEEP TIME
                IN ORDER TO HAVE SYNCHRONIZATION
                CONSIDERING THE WORST CASES
                DO NOT WRITE CASUAL TIME AND
                AND HOPE TO WORK
            */
#if DEBUG==1
        fprintf(stdout, "Parent process running\n");
#endif 
            sleep(3);
            printf("Father Woke-up\n");
        }
            

    }

    return 0;
}