#include<sys/wait.h>
#include <unistd.h>
#include "handle.h"

#define DEBUG 1
#define N_RD 2
#define WRBIN 0


int main(int argc, char **argv){
    int  fd, n, n_rd;
    int  pid, childPid, statVal;
    int  val[N_RD];


    setbuf(stdout, 0);
    setbuf(stdin, 0);
    if(argc != 2){
        fprintf(stderr, "Invalid input arguments\n");
        exit(EXIT_FAILURE);
    }





#if WRBIN==1
    n = writeBin(argv[1]);
    printf("FILE BEFORE ORDERING IN WRBIN\n");

    fd =open(argv[1], O_RDONLY);
    while(read(fd,&n_rd, sizeof(int))>0){
        printf( "%d\t", n_rd);
    }
    printf("\n");
    close(fd);
#elif DEBUG==1
    printf("FILE BEFORE ORDERING\n");

	fd =open(argv[1], O_RDONLY);
	while(read(fd,&n_rd, sizeof(int))>0){
		printf( "%d\t", n_rd);
	}
	printf("\n");
	close(fd);
#endif


    fd = open(argv[1], O_RDWR);
    if(fd == -1){
        printf("File not found\n");
        exit(EXIT_FAILURE);
    }
    //MANAGEMENT OF THE FST W
    //use to over write file already written
    lseek(fd, 0, SEEK_SET);
    if(read( fd, &n,sizeof(int) <= 0 ) ){
        fprintf(stderr, "Error reading file\n");
        exit(EXIT_FAILURE);

    }
#if DEBUG==1
    printf("n=%d\n", n);
#endif

    //start from 1 'cause the fst it's for know num
    //FIXED the error was the forgetfulness OF A FUCKING -1 before n
    //MINUS -1
    for (int i = 0; i < n-1; ++i){
        //start from j=0+1=1 'cause the fst it's for know num
        //and obviously it ends at n-1-i+1=n-i
        for(int j=1; j<n-i; ++j){
            pid = fork();
            if(pid ==-1){
                fprintf(stderr, "Error process creation\n");
                exit(EXIT_FAILURE);
            }
            else if(pid>0){
                //parent
                childPid=wait(&statVal);
#if DEBUG==1
                if(WIFEXITED(statVal))
                    printf("child pid=%d\texit value=%d\n", childPid, WEXITSTATUS(statVal));
                else
                    printf("child pid=%d ABNORMAL termination\n", childPid);
#endif

            }
            else{
                if( random_swap(fd, j, val, N_RD) ==EXIT_FAILURE)
                    exit(EXIT_FAILURE);
#if DEBUG==1
                printf("FILE SWAP CHILD(%d)\n", getpid());
                lseek(fd, 0, SEEK_SET);
                while(read(fd,&n_rd, sizeof(int))>0){
                    printf( "%d\t", n_rd);
                }
                printf("\n");
                close(fd);
#endif
                exit(EXIT_SUCCESS);
            }
        }
    }

    close(fd);

#if DEBUG==1
    printf("FILE AFTER ORDERING\n");
    fd =open(argv[1], O_RDONLY);
    while(read(fd,&n_rd, sizeof(int))>0){
        printf( "%d\t", n_rd);
    }
    printf("\n");
    close(fd);
#endif

    return 0;
}
