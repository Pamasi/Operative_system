#ifndef  HANDLE_H
#define HANDLE_H

#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>


int writeBin(char* file_name);
int random_swap(int fd, int i, int *val, const int DIM);

#endif
