#include "handle.h"
#include <fcntl.h>

int writeBin(char* file_name){
    int fd, num=-1 , elem;

    //open file
    fd = open(file_name,  O_WRONLY );

    if(fd==-1){
        fprintf(stderr, "file not found!\n");
        exit(EXIT_FAILURE);
    }

    while(num<0){
        printf("Insert number of element to write:");
        scanf("%d", &num);
        printf("\n");
    }
    //print number  to read to the fst file
    if(write(fd, &num, sizeof(int)) <=  0 ){
        fprintf(stderr, "file writing error!\n");
        close(fd);
        exit(EXIT_FAILURE);
    }

    for(int i=0; i<num; i++){
        fscanf(stdin, "%d", &elem);

        if( write(fd, &elem, sizeof(int)) <= 0){
            fprintf(stderr, "file writing error!\n");
            close(fd);
            exit(EXIT_FAILURE);
        }
    }

    printf("Operation done\n");
    close(fd);

    return num;

}

int random_swap(int fd, int i, int *val, const int DIM){
    int tmp;

    lseek(fd, i*sizeof(int), SEEK_SET);
    if(read(fd, val, DIM*sizeof(int)) <= 0){
        fprintf(stderr, "Error reading file\n");
        exit(EXIT_FAILURE);
    }

    if(val[0]>val[1]){
        tmp = val[0];
        val[0] = val[1];
        val[1]	= tmp;

        //reset pos in order to write
        //remember that read and write syscall
        //change the file pos on fd
        lseek(fd, i*sizeof(int), SEEK_SET);
        if(write(fd, val, DIM*sizeof(int)) <= 0 ){
            fprintf(stderr, "Error writing file\n");
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;

}