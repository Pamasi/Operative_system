#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<unistd.h>
#include<signal.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<assert.h>
#include<stdbool.h>

#define N 20
#define DEBUG 0

static const char pidFile[]= "pid.txt";
static const char file_exchange[] = "pid.txt";
static unsigned int nprocess = 2;

static void s_handler(int sig);
static void consumer(unsigned int prod_pid);
static void producer();


int main( int argc, char **argv){
    int pid, prod_pid;

    setbuf(stdin,0);
    setbuf(stdout,0);

    // declaration of signal handler
    if( signal(SIGUSR1, s_handler) == SIG_ERR ||
        signal(SIGCHLD, s_handler) == SIG_ERR 
         ){
        fprintf(stderr, "Error assigning signal handler\n");
        return(1);
    }


    // REMEMBER: fork
    //          return 0 if child, 
    //                 childPid if parent
    //handle fst child (producer)
    pid =fork();

    if(pid==-1){
        fprintf(stderr, "Fork ERROR\n");
        exit(EXIT_FAILURE);
    }
    if(pid>0){
        // create consumer
        prod_pid = pid;
        pid = fork();

        if(!pid){
            consumer(prod_pid);
            kill(getppid(), SIGCHLD);
            exit(EXIT_SUCCESS);
        }
        else{
            while(nprocess>0);
        }
    }
    //parent
    else{
        producer();
        kill(getppid(), SIGCHLD);
        exit(EXIT_SUCCESS);
    }
  
#if DEBUG==1 
printf("Exit  pid=%d\n", getpid()); 
#endif
}

/*FUNCTIONS*/

static void s_handler(int sig){
    switch (sig)
    {
        case SIGUSR1:
#if DEBUG==1
    printf("\nProcess pid=%d caught SIGUSR1\n", getpid());
#endif
            break;

        case SIGCHLD:
            nprocess--;
            break;

        default:
            printf("Wrong signal caught\n");
            exit(EXIT_FAILURE);
    }

}

static void consumer(unsigned int prod_pid){
    unsigned int  pid;
    int fd_pid, fd, end=1;
    char buf[N];
    //write pid to file
    fd_pid = open(pidFile, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR |S_IWUSR| S_IROTH);

    pid = getpid();

    if ( write(fd_pid, &pid,sizeof(int) ) <= 0){
        fprintf(stderr, "Writing  consumer pid ERROR\n");
        exit(EXIT_FAILURE);
    }
    
    close(fd_pid);

    //send permission producer to work
    kill(prod_pid, SIGUSR1);
    
    do{ // start work after receiving signal from producer
        pause();
        fd = open(file_exchange, O_RDONLY);     
        while( read(fd, buf, sizeof(buf)) > 0 ){

            end = strcmp(buf, "end");
            if(end){
                for(int i =0; i< N; ++i )
                    buf[i] = toupper(buf[i]);

                printf("modified string:%s\n", buf);
                fflush(stdout);
            }
        }     
        close(fd);
        // use to start communication
        if(end)  kill(prod_pid, SIGUSR1);
    }while(end);
}

static void producer(){
    int fd_pid, fd;
    unsigned int cons_pid;
    char buf[N];
#if DEBUG==1
printf("Producer pid=%d\n", getpid());
#endif

    // wait producer signal to start
    pause();

    // since the consumer know the producer pid
    // only the consumer write its pid in file
    // and producer read from file
    // read consumer pid
    fd_pid = open(pidFile, O_RDONLY);

    if( read(fd_pid, &cons_pid, sizeof(int)) <= 0){
        fprintf(stderr,  "ERROR: not possible to read pid consumer by producer\n");
        exit(EXIT_FAILURE); 
    }
    close(fd_pid);

    do{
        printf("Insert a string\n");
        scanf("%s", buf);
        // transfer str to child consumer by means of a file
        fd = open(file_exchange, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR |S_IWUSR| S_IROTH);
        
        if( write(fd, buf, sizeof(buf)) < strlen(buf)){
            fprintf(stderr, "Writing  producer ERROR\n");
            exit(EXIT_FAILURE);
        }  

        close(fd);
        // send signal: consumer start converting
        kill(cons_pid, SIGUSR1);

        //use sleep to synchronize the sibling process  
        if(strcmp(buf, "end")) pause();
  
    }while(strcmp(buf, "end"));
}