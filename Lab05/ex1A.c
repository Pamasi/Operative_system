#include<stdio.h>
#include<stdlib.h>

int main(){
	int num;

	setbuf(stdout, 0);
	fscanf(stdin, "%d", &num);

	while(num!=0){
		if(num%2==0)
			fprintf(stdout, "%d\n", num);
		else
			fprintf(stderr, "%d\n",num);

		setbuf(stdout, 0);
		fscanf(stdin, "%d", &num);

	}
	/*
	 *from cli ./ex01 <inFile.txt 1>evenFile.txt 2>oddFile.txt 
	 * */
}
