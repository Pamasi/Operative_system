#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<unistd.h>
#include<signal.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<assert.h>
#include<stdbool.h>

#define N 20

static const char pidFile[]= "pid.txt";
static const char file_exchange[] = "pid.txt";
static unsigned int nprocess = 2;

static void s_handler(int sig);
static void consumer();
static void producer();


int main( int argc, char **argv){
    int pid;

    setbuf(stdin,0);
    setbuf(stdout,0);

    // declaration of signal handler
    if( signal(SIGUSR1, s_handler) == SIG_ERR ||
        signal(SIGCHLD, s_handler) == SIG_ERR 
         ){
        fprintf(stderr, "Error assigning signal handler\n");
        return(1);
    }

    //handle fst child (producer)
    pid =fork();

    if(pid==-1){
        fprintf(stderr, "Fork ERROR\n");
        exit(EXIT_FAILURE);
    }
    if(pid>0){
        // create consumer
        pid = fork();

        if(!pid){
            consumer();
            // kill(getppid(), SIGCHLD);

            // exit(EXIT_SUCCESS);
        }
        else{
            while(nprocess>0);
        }
    }
    //parent
    else{
        producer();
        exit(EXIT_SUCCESS);
    }
  
    printf("Exit  pid=%d\n", getpid());
}

/*FUNCTIONS*/

static void s_handler(int sig){
    switch (sig)
    {
        case SIGUSR1:
            printf("\nProcess pid=%d caught SIGUSR1\n", getpid());
            break;

        case SIGCHLD:
            nprocess--;
            break;

        default:
            printf("Wrong signal caught\n");
            exit(EXIT_FAILURE);
    }

}

static void consumer(){
    int pid, prod_pid;
    int fd_pid, fd;
    char buf[N];
    //write pid to file
    fd_pid = open(pidFile, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR |S_IWUSR| S_IROTH);

    pid = getpid();

    if ( write(fd_pid, &pid,sizeof(int) ) <= 0){
        fprintf(stderr, "Writing  consumer pid ERROR\n");
        exit(EXIT_FAILURE);
    }
    
    close(fd_pid);
    
    // waiting letting producer doing its job
    pause();

    fd_pid = open(pidFile, O_RDONLY);

    // read producer pid
    // that it's written at the end of the file
    lseek(fd_pid, -1*sizeof(int), SEEK_END);
    if ( read(fd_pid, &prod_pid,sizeof(int) ) <= 0){
        fprintf(stderr, "ERROR Reading  producer pid \n");
        exit(EXIT_FAILURE);
    }

    close(fd_pid);

    //send a signal to producer to start work
    kill(prod_pid, SIGUSR1);
    
    while(true){
        // start work after receiving signal from producer
        pause();

        fd = open(file_exchange, O_RDONLY);     
        while( read(fd, buf, sizeof(buf)) > 0 ){
                for(int i =0; i< N; ++i )
                    buf[i] = toupper(buf[i]);

                printf("modified bufing:%s\n", buf);
        
                fflush(stdout);
        }     
        close(fd);
        // use to start communication
        kill(prod_pid, SIGUSR1);
    }
}


static void producer(){
    int fd_pid, fd, cons_pid;
    char buf[N];
    printf("Producer pid=%d\n", getpid());
 
    // wait consumer to finish its job
    // FIXME:HOW TO MANAGE THIS PART WITHOUT SLEEP?
    sleep(1);
    // read consumer pid
    fd_pid = open(pidFile, O_RDWR);

    if( read(fd_pid, &cons_pid, sizeof(int)) <= 0){
        fprintf(stderr,  "ERROR: not possible to read pid consumer by producer\n");
        exit(EXIT_FAILURE); 
    }


    //write producer pid
    int prod_pid= getpid();
    
    if( write(fd_pid, &prod_pid, sizeof(int)) <= 0){
        fprintf(stderr,  "ERROR: not possible to write producer\n");
        exit(EXIT_FAILURE); 
    }


    close(fd_pid);

    // wait for signal from consumer to start
    // pause();
    kill(cons_pid, SIGUSR1);
    pause();

    printf("Insert a string\n");
    scanf("%s", buf);


    while(strcmp(buf, "end")){
        // transfer str to child consumer by means of a file
        fd = open(file_exchange, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR |S_IWUSR| S_IROTH);
        
        if( write(fd, buf, sizeof(buf)) < strlen(buf)){
            fprintf(stderr, "Writing  producer ERROR\n");
            exit(EXIT_FAILURE);
        }  

        close(fd);
        // send signal
        kill(cons_pid, SIGUSR1);
        //use sleep to synchronize the sibling process  

        pause();

        printf("Insert a string\n");
        scanf("%s", buf);  
    }

    kill(cons_pid, SIGKILL);

    //notify the  parent the termination of a children
    kill(getppid(), SIGCHLD);
}