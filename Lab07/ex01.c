#include <stdio.h>
#include <stdlib.h>
#include<unistd.h>
#include<math.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<sys/types.h>


int main (int argc, char *argv[]){
    int n, pid;
    int *vet;

    n = atoi (argv[1]);

    vet = (int *)malloc(n * sizeof(int));
    if (vet == NULL) {
        printf("Allocatin Error.\n");
        exit(1);
    }

    printf("Binary Numbers:\n");


    for (int i = 0; i < n; i++)
    {
        pid = fork();

        if(pid ==-1)
            exit(EXIT_FAILURE);
        else if(pid>0){
            vet[i] =0;
            wait(NULL);
        }
        else{
            vet[i]=1;
        }
    }


    // synchronize print
    setbuf(stdout, 0);

    // sleep(2);
    for (int j=0; j<n; j++) {
        printf("%d", vet[j]);
    }
    printf("\n");
    free(vet);

    return 0;
}





