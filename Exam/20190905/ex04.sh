#!/bin/bash
file=$(sudo find  . -name "virus.dat" ) 

if [ -z $file ]; then
    echo -e "ERROR: virus.dat not found\n"
    exit 1
fi

# use tmp file
while read pid; do
    # TIPS:  DON'T USE REGEX WITH TR AND CUT THEY CAN LEAD TO STRANGE BEHAVIOUR
    res=$(ps -ef | tr -s ' ' |cut -d ' '  -f 1-3 | grep -E ".*\s$pid\s[0-9]+.*" )

    if [ "$res"  ]; then
        # read owner <$res wrong 'cause not a file
        owner=$(echo $res | cut -d ' ' -f 1)
        echo -e "Process pid=$pid\nowner:$owner"

        child=$(echo $res | cut -d ' ' -f 3 )
        if [ "$child" ]; then  # not empty
            echo  "child pid are:$child"
        else 
            echo  "no child\n"
        fi
    fi 

done <$file