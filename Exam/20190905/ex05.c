#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>

#define R 3
#define C 2
#define TRUE 1

static int matrix[R][C];
static sem_t sem[R];



void *read(void *arg){
    int row = *((int*) arg);

    sem_wait(sem +row);
    printf("row%d\n", row);

    for (int i = 0; i< C; ++i)
        scanf("%d", &(matrix[row][i]) );
    
    
    if(row<R-1){
        sem_post(sem+row+1);
    }
    else{
        sem_post(sem);
        printf("\n");
    }


    pthread_exit(NULL);
}

void *write(void *arg){
    int row= *((int*) arg);

    sem_wait(sem+row);

    for (int i = 0; i< C; ++i)
        printf("%d\t", matrix[row][i]);
    printf("\n");

    if(row<R-1)  sem_post(sem+row+1);

    pthread_exit(NULL);
}

int main(){
    pthread_t tr[R], tw[R];
    int row[R];
 
    setbuf(stdout, 0);   // change also to every TCB since the same process
    setbuf(stdin, 0);
    sem_init(sem,0,1);

    for(int i=1; i<R; ++i )
        sem_init(sem+i,0,0);    
    
    /** REMEMBER pointer act as shar variable 
     * so if main change them threads see it:
     * this is way I used an array row
     * and not i*/
    for(int i=0; i<R; ++i){
        row[i]=i;
        pthread_create(tr+i, NULL, read, (void*)&row[i]);
    }

    for(int i=0; i<R; ++i)
        pthread_join(tr[i], NULL);


    for(int i=0; i<R; ++i){
        row[i]=i;
        pthread_create(tw+i, NULL, write, (void*)&row[i]);
    }


    for(int i=0; i<R; ++i)
        pthread_join(tw[i], NULL);

    // DO WE HAVE TO DO EVERY  CONTROL IF 
    // IF IT IS NOT EXPLICITLY REQUIRED?
    sem_destroy(sem);


    return 0;
}