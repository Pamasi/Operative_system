/** REPORT THE BODY OF P1, .., P8*/
#include<stdio.h>
#include"semaphore.h"
#include<unistd.h>
#include<fcntl.h>


static int NS 8;
static semaphore *S;

int main(){

	S = (semaphore*)malloc(NS*sizeof(semaphore));
	init(S[0], 1);

	for( int i=0; i<NS; ++i)
		init(S[i], 0)
			
			for( int i=0; i<NS; ++i)
				init(S[i], 0);;
	//... creation of process
	//destroy semaphore
	for(int i=0; i<NS; ++i)
		destroy(S[i]);
	free(S);
}

/* body of processes*/
process 1
while(1){
	wait(S[0]);
	P1();
	signal(S[1]);
	signal(S[2]);
}


process 2
while(1){
	wait(S[2]);
	P2();
	signal(S[3]);
	signal(S[4]);
	signal(S[5]);
}

process 3
while(1){
	wait(S[3]);
	P3();
	signal(S[7]);

}

process 4
while(1){
	wait(S[4]);
	P3();
	signal(S[7]);

}

process 5
while(1){
	wait(S[5]);
	P3();
	signal(S[7]);

}

process 7
while(1){
	for(int i =0; i<3; ++i) wait(S[7]);
	P7();
 	signal(S[8]);	
}


process 6
while(1){
	 wait(S[1]);
	 P6();
	 signal(S[8]);
}

process 8
while(1){
	wait(S[8]);
	wait(S[8]);
}
