!/bin/bash

if [ $# -ne 2 ]; then
	exit 1
fi

path=find . -regex $1;

if [ -z "$path" ]; then
	exit 2
fi

let out=0

while [ $out -eq 0 ]; do
	sleep $2
	let zombie=0
	while read line; do
		
		tmp=$(ps -el | cut -d " " -f 2,14 | grep -e $line)
		if [ -z "$tmp" ]; then
			let $out=1
			break;
		else
			let i++
		fi
		let tmp2=$(echo $tmp | cut -d " " -f 1 | grep -e "Z" -n)
		let zombie+=$tmp2
		live[i]=$(echo $tmp | cut -d " " -f 2)
	
	done <$path

	
	printf "number of zombie=%d\n" $zombie
	if [ $out -eq 1 ]; then
		echo "all process dead"
	else
		echo -e "live process:\n"
		for ((i=0; i<$n; i++)); do
			echo -e "${live[i]}\n"
		done
	fi

done
