Total 796 refers to the number of regular file, special file present in the directory (796).
The first column shown by the command ls represent if a file is a directory(d) or regular file(-) or a link (l) and the others characters  use-group-other read-write-execute permission written in triplet.
The second field represents the number of instances of that file in the file system. 
The third and fourth fields represent respectively the owner and group.
The fifth field represents the file dimension.
The sixth and seventh fields represent last modification date.
Finally, the last field represents the name of the file/link/directory, "." indicate the current directory, while ".." indicates the parent directory.

Directory entry is a entry that represents directory its contains all information to file/link/directory inside it. The information related to file change according to the implementation of the file system.
The i-node is a table present for each file in the file system where are stored all pointers
related to different chunks of a particular file in the external memory, each file has its owntable(i-node).
Hard-link is particular link in UNIX system that links to directly to i-node related to a file (its has the same i-node) and has  counter(each link has its counter) whose value dependto the counter related to that file(link counter=counter file+1):at each link removal the counter of the file is decremented; the original file can be removed from memory only when  the counter is equal to 0, meaning that there is only one link to the particular i-node file.

Creation of an hard link:
ln file_name link_name_path

Soft-link is a particular link in UNIX system that link to a i-node related of a file, basically the soft-link i-node is a copy of the original i-node file. When the file is deleted soft-lins remain in a dandling state, UNIX system tells are point to nothing.

ln -s file_name link_name_path
