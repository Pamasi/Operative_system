#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

static int n;
static pthread_mutex_t lock;

void *routine(void *arg){
	pthread_t *t;
	int num=*((int*)arg), a;
	unsigned long int tid;

	tid=pthread_self();
	pthread_detach(tid);
	pthread_mutex_lock(&lock);
	a=--n;
	pthread_mutex_unlock(&lock);

	if(num%2){
		t=(pthread_t*)malloc(2*sizeof(pthread_t));
		pthread_create(t,NULL, routine,	(void*)&a);
		pthread_create(t+1, NULL, routine, (void*)&a);	
	}
	else{
		t=(pthread_t*)malloc(sizeof(pthread_t));
		pthread_create(t, NULL, routine, (void*)&a);
	}
	
	if(a==0){
	
		printf("tid=%lu",tid);
	}

	free(t);
	pthread_exit(NULL);
}


int main(int argc, char** argv){

	int a=1;
	pthread_t t;

	if(argc!=2) exit(-1);
	n=atoi(argv[1]);

	pthread_mutex_init(&lock, NULL);
	pthread_create(&t, NULL, routine, (void*)&a);
	pthread_mutex_destroy(&lock);
	return 0;

}

