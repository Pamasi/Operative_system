#1/bin/bash

if [ $# -ne  1 ]; then
    exit 1
fi

if [ -z $(find . -name $1) ]; then
    exit 2
fi

let fst=0
declare -A node
let weight=0
# to form a graph 
# a node must be present at in 2 line
while read line; do
    if [ $fst -eq 0 ]; then
        let count=0
        for e in line; do
            if [ count -lt 3 ]; then
                let node[$e]++
                let count++
            else
                weight+=$e
            fi
        done;
done <$1

let count=0

path=${#node[*]}
let path*=2
for n in node; do
    if [ n -ge 2 ]; then
        let count++
    fi
do

if [ $count -gt $path ]; then
    echo -e "wrong path\n"
    exit 3
elif 
    echo -e "correct path, total weight $weight"
fi


unset node