#include<stdlib.h>
#include<stdio.h>
#include<pthread.h>
#include<semaphore.h>
#include<time.h>
#include<unistd.h>

static int *buffer;
static sem_t *s_buf;
static pthread_mutex_t mux;
static pthread_t *t;


typedef struct{
    int n,p, tid; 
}arg_t;

void *write_t(void *args){
    int t_sleep, n_buf;
    arg_t *a=(arg_t*)args;
    t_sleep= rand()%3;
    // size between n e 
    n_buf= rand()%(a->n-1);
    
    sleep(t_sleep);
    sem_wait(s_buf+ n_buf);
    buffer[n_buf] = a->tid;
    sem_post(s_buf);

    free(args);

    pthread_exit(NULL);
}


void *read_t(void *args){

    int n_buf=rand() %(*((int*)args)-1);

    sem_trywait(s_buf+n_buf);
    printf("buffer[%d]=%d\n",n_buf, buffer[n_buf]);
    sem_post(s_buf+n_buf);

    pthread_exit(NULL);
}

void *handler(void *args){
    arg_t *a= (arg_t*) malloc(sizeof(arg_t));

    a->n= ((arg_t*)args)->n;
    a->p= ((arg_t*)args)->p;

    buffer=(int*) malloc(a->n*sizeof(int));

    for(int i=0; i<a->n; buffer[i++]=-1);
    for(int i=1; i<a->p; ++i){
        a->tid= i;
        pthread_create(t+i, NULL, write_t, (void*) a);
        sem_init(s_buf+i-1, 0, 1);
    }

    pthread_create(t+a->p, NULL, read_t, (void*)&(a->n));

    pthread_exit(NULL);
}



int main(int argc, char **argv){

   
    arg_t a;
    if(argc!=2) exit(EXIT_FAILURE);

    a.n = atoi(argv[1]);
    a.p = atoi(argv[2]);



    t=(pthread_t*)malloc((a.p+2)*sizeof(pthread_t));
    s_buf = (sem_t*) malloc((a.p)*sizeof(sem_t));


    pthread_create(t,NULL, handler, (void*)&a);

    sleep(5);
    for(int i=1; i<a.p+1; ++i){
        if(pthread_join(t[i], NULL)<=0)
            exit(EXIT_FAILURE);
    }

    free(buffer);
    free(s_buf);

    return 0;
}