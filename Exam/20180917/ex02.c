#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>

int main( int argc, char **argv){
    int pid, n;
    if(argc!=2) exit(EXIT_FAILURE);

    n = atoi(argv[1]);
    
    for(int i=0; i<n; ++i){
        pid=fork();
        if(pid<0) exit(EXIT_FAILURE);
        if(!pid) exit(EXIT_SUCCESS);
    }
    printf("hello\n");
    sleep(60);
    while(1>0){ printf("***\r"); }
    // for(int i=0; i<n; ++i)
    //     wait(NULL); // waitpid(P_PID,pid, WEXITED); not needed

    
    return 0;
}