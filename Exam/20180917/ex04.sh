#!/bin/bash

if [ $# -ne 1 ]; then
    echo -e"Error num params\n"
    exit 1
fi

res=$(find  -regextype posix-extended -regex "$1/(.*-+.*)+.c" )

if [ -z "$res" ]; then
    echo -e "No file\n"
    exit 2
else
    for path in $res; do 
        dir=$(dirname $path)
        file=$(basename $path)
        tmp=$(echo $file| tr - / )

        dirnew=$(dirname $tmp) #a/b/c

        file=$(basename $tmp) #foo.c

        if [ -z "$(find $dir/$dirnew)" ]; then
            # -p option create parents if do not exist
            mkdir -p $dir/$dirnew
        fi

        cp $path $dir/$dirnew/$file
    done
fi

exit 0