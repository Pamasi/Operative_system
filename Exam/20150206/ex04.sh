#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Invalid params num"
    exit 1
fi


# I have decide to show dimension
let fst=0
while read line; do
    echo $line | grep -e "^total" &> /dev/null
    if [ $? -eq 0 ]; then  # if last cmd exit 0 means if finish correctly
        if [ $fst -ne 0 ]; then
            echo -e "max file lenght=$max_dim\n"
        fi
        let max_dim=0
        let fst=1

    else
        # if it is a dir skip
        # -v indicate invert ( not-matching lines)
        tmp_dim=$(echo $line | grep -e "^d" -v | cut -d " " -f 5)
        # it 
        if ! [ -z $tmp_dim ] && [ $tmp_dim -ge $max_dim ]; then
                max_line=$line
                max_dim=$tmp_dim
        fi
    fi
    
done <$1

echo  "max file lenght=$max_dim"
exit 0