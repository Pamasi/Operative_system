#!/bin/bash

# suppose that user onyl to use directory from its working dir
if [ $# -ne 1 ]; then
    echo-n "wrong params\n"
    exit 1
fi
res=$(find . -name $1 -type d)

if [ -z "$res" ]; then
    echo -n "dir not exits\n"
    exit 2
fi


echo  "$(find $res -type f -exec wc -c \{} \; | cut -d " " -f 1 | sort -n )"

for dim in $(find $res -type f -exec wc -c \{} \; | cut -d " " -f 1); do
    let remainder=$dim%1024
    let index=$dim/1024

    if [ $remainder -ne 0 ] || [ $index -eq 0 ]; then
        let index++
    fi

    let histo[$index]++

done

##REMIND:  with let expression do not use $ for uninary operator
for ((i=1; i<=${#histo[*]}; ++i)); do
    printf "%s\t" $i
    let max=histo[$i]
    while [ $max -gt 0 ]; do
        printf "%s" "#"
        let max--
    done

    printf "\n"
done





exit 0