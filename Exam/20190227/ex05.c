#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>

sem_t *S;
int *sequence;

void *fibonacci(void *args){
    int pos= *((int*)args);

    sem_wait(S+pos);

    if(pos==0 || pos==1)
        sequence[pos]=1;
    else 
        sequence[pos]=sequence[pos-1]+sequence[pos-2];
    
    printf("%d\n", sequence[pos]);

    sem_post(S+pos+1);
    pthread_exit(NULL);
}
int main(int argc, char **argv){
    pthread_t *tid;
    int n;
    int *args;
    if(argc!= 2 ){
        fprintf(stderr,"NO INPUT!!!!!!!!!!!!!!!\n");
        exit(EXIT_FAILURE);
    }

    n= atoi(argv[1]);
    tid=(pthread_t*)malloc(n*sizeof(pthread_t));
    args=(int*)malloc(n*sizeof(int));
    sequence=(int*)malloc(n*sizeof(int));
    S=(sem_t*)malloc(n*sizeof(sem_t));
    sem_init(S,0,1);
    for( int i=0; i<n; ++i){
        if(i>0)
            sem_init(S+i, 0, 0);
        args[i]=i;
        pthread_create(tid+i, NULL, fibonacci,&args[i]);
    }

    for(int i=0; i<n; pthread_join(tid[i++],NULL) );
    free(tid); free(args); free(sequence);
    sem_destroy(S);
    return 0;
}