#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>

#define DEBUG 0 // use 1 to debug

typedef struct{
    int r,l;
    int res;
}file_t;

void *sum(void *arg){
    ((file_t*)arg)->res = ((file_t*)arg)->l +((file_t*)arg)->r;

#if DEBUG==1
    printf("res=%d\t left=%d\t right=%d\n", ((file_t*)arg)->res, 
        ((file_t*)arg)->l,  ((file_t*)arg)->r);
#endif

    pthread_exit(NULL);
}


int array_sum(int *vet, int n)
{
    // concurrent version
    int k, tmp[n];
    pthread_t *tid;
    file_t *arg;

    // cpy vector
    for(int i=0; i<n; ++i) tmp[i]=vet[i];

    k = n/2;
    while(k>=1)
    {
        tid = (pthread_t*) malloc(k*sizeof(pthread_t));
        arg = (file_t*) malloc(k*sizeof(file_t));
        for(int i=0; i<k; ++i)
        {
            // sum
            arg[i].l=tmp[i]; arg[i].r=tmp[i+k];
            pthread_create(tid+i, NULL, sum, (void*)(arg+i));

        }
        // wait to finish sum
        for(int i=0; i<k; ++i){
            pthread_join(tid[i], NULL);

            // tmp invalid
            tmp[i+k] = -1;

            // resize without malloc, more efficient?
            tmp[i] = arg[i].res;

            #if DEBUG==1
                printf("stem=%d\t tmp[%d]=%d\n", k, i, tmp[i]);
            #endif
        }
            
        //free
        free(tid); free(arg);
        k/=2;

    }

    



    return tmp[0];
}


int main(int argc, char **argv)
{
    int *vet, n;
    if(argc !=2){
        fprintf(stderr, "ERROR: invalid num params\n");
        exit(EXIT_FAILURE);
    }
    n= atoi(argv[1]);
    vet = (int*)malloc(n*sizeof(int));
    printf("Insert number:\n");

    for(int i = 0; i<n; ++i)
        scanf("%d", &vet[i]);

    

    printf("res=%d\n", array_sum(vet, n));
    free(vet);
    
    return 0;
}