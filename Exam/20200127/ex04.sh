#!/bin/bash
if [ $# -ne 1 ]; then
    echo -e "Invalid num arg\n"
    exit 1
fi


# remember -name -> complete str
# remember -regex -> partial str BUT SPECIFY THE PATH
# search file
file=$(sudo find $1 -regex "\./expense.*" -regextype posix-extended -type f  -size 10M)


# use "" around $file because file is a list of str?
if [ -z "$file" ]; then
    echo -e "No file found\n"
    exit 3
fi

# select all rgular file with dim <10MB
declare -A product
declare -A tmp

echo -e "start reading...\n"
for f in $file; do 
    echo -e "$f\n"

    # ignore the fst line read
    while read line; do
        let ignore=0
        if [ $ignore -ne 0 ]; then
            for str in line; do
                let i=0
                tmp[$i]=$str
                let i++
            done

            if [ $i -ne 3 ]; then
                echo -e "ERROR: invalid format\n"
                exit 2
            fi

            if [ -z ${product[ ${tmp[0]} ]} ]; then
                let product[${tmp[0]}]=product[${tmp[1]}]\
                    *product[${tmp[2]}]
            else 
                let product[${tmp[0]}]+=(product[${tmp[1]}]\
                  *product[${tmp[2]}])

            fi
        else 
            let ignore++
        fi


    done <$f

done 


# deallocate
unset $product
unset $tmp