Signal in UNIX system are software interrupts
used to permit comunication between processes.
This is possible using the syscall signal and kill.
The signal syscall receive the signalcode as an integer
as the signal handler functioon, whose job its to hadnler the signalbased on 
signal code(integer) receive as parameter.
In case of error signal return SIG_ERROR.
The prototipe is the following
void *signal(int signalCode, void *(sigHandler(int)))

The syscall kill has the following prototipe:

int kill(pid_t pid, int signalCode)

where the first parameter is the pid and second one
is the signal to send the pid process; this syscall 
return -1 in case of error.

-------------------
first example
-------------------
#include<signal.h>
#include<unistd.h>
static int normal=0;
void handler(int signal){
 return;
}

void alarm(unsigned int time){

    int pid;

    if(signal(SIGALARM, handler )==SIG_ERR ||
        signal(SIGUSR2, handler) == SIG_ERR)
        exit(-1);

    pid=fork();
    if(pid==0){
        sleep(time);
        kill(getppid(), SIGALRM);
        exit(EXIT_SUCCESS);
    }
    else{
        pause();
    }

    exit(EXIT_SUCCESS);
}

----------------------
snd example
----------------------
#include<signal.h>
#include<unistd.h>

static char msg[30];
void handler(int signal){

    if( SIGALARM)
        printf("%d",msg);
}


int main(){

    if(signal(SIGKILL, handler)==SIG_ERR) exit(-1);
    scanf("%s", msg);
    alarm(10);

}