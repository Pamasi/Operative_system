#!/bin/bash

if [ $# -ne 1 ]; then
    echo "dfg"
    exit 1
fi

let n_file=0
while read path; do 

    user=$(whoami)
    if [ -f $path ]; then
        file_n=$(basename $path)
        dir_n=$(dirname $path)
        res=$(cd $dirname; ls -l | tr -s " " |\
            cut -d " " -f 1,3,5,9 | \
            grep -E "^-.*\s$user\s[0-9]*001\s$file_n")

        echo $res

        if ! [ -z $res ]; then
            let n_file++
        fi

    elif [ -d $path ]; then
        res=$(cd $path; ls -l | tr -s " " |\
        cut -d " " -f 1,3,5,9 | grep -E "^-.*\s$user\s[0-9]*001\s.*")
        
        for i in $res; do
            let n_file++
            tmp_row=$(echo $i | cut -d " " -f 4 )
            let n_row+=tmp_row
        done
       
        let n_row+=tmp_file

    fi

done <$1

echo "number file=$n_file"
echo "number row=$n_row"

exit 0