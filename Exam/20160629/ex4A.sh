#!/bin/bash
if [ $# -ne 1 ]; then
    exit 1
fi
let num=0
let n_row=0

while read line; do
    # line=$(echo $line | tr -d "\n") useless read line do no mem \n
    path=$(dirname $line)
    name=$(basename $line)
    user=$(whoami)
    
     # find  $path -name $name -type d -size +101c -user $user &> /dev/null
     # you cant't use the above instruction since it even there nothing to print it return 0
    res=$(find  $path -name "$name" -type d -size +101c -user $user)

    # if is a dir
    if ! [ -z $res ]; then
        tmp=$(find $line -name "*" -type f -size +101c -maxdepth 1  \
         -user $user -exec wc -l \{} \; | cut -d " " -f 1)
    
        tmp_file=$(echo $tmp | wc -l);

        for e in $tmp; do
            let n_row+=e
        done
         
        echo -e "dir=$line\tn_file=$tmp_file\n"
        let n_file+=file_tmp
       
    else
        res=$(find $path -name $name -type f -size +101c -user $user)
    
        if ! [ -z $res ]; then
            let n_file++
            tmp=$(cat $line | wc -l)
            let n_row+=tmp
        fi
    fi

done <$1

echo "num file=$n_file"
echo "num row=$n_row"

exit 0