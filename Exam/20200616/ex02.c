/* OPEN QUESTION
The charactericts of the signal to send asynchronize information
are:
-signal creation by a process using syscall kill, raise, alarm
-signal delivery by the kernel to appropriate process
-reaction to signal by the process receives it, basically
by using accepting the
default behaviour defined for that signal
or using a custom signalHandler specified by means of 
function signal(int signal, void *sigHandler).

*/

/*PROGRAM*/
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<signal.h>

#define TRUE 1
static int sig, p1=0;
static void handler(int signal){
    if(signal==SIGUSR1){
        printf("Signal received from P1\n");
        p1==0?(sig=1):(++sig);
        p1=1;
    }
    else if(signal==SIGUSR2){
        printf("Signal received from P2\n");
        p1==0?(++sig):(sig=1);
        p1=0;
    }
    

}

int main(){
    int p1pid, p2pid;
    signal(SIGUSR1, handler);
    signal(SIGUSR2, handler);

    p1pid=fork();
    if(p1pid){
        p2pid=fork();
        if(!p2pid){
            while(TRUE){
                sleep(rand()%100+20); // [20,100]
                kill(getppid(), SIGUSR2);
            }
        }
        else{
            while(TRUE){
                pause();
                if(sig==3){
                    kill(p1pid, SIGKILL);
                    kill(p2pid, SIGKILL);
                }
            }
        }
        
    }
    else{
        while(TRUE){
            sleep(rand()%100);
            kill(getppid(),SIGUSR1);
        }
    }


    return 0;

}