/**
 *  Using the system calls fork and wait,
 *  realize the following precedence graph.

        P1
       /  \
      /    \
    P2      P3
    /\      /\
   /  \    /  \
 P4   P5  |   P6
   \  /    \  /
    \/      \/
    P7      P8
      \    /
       \  /
        P9

Note that all arcs are oriented downwards.
Each Pi process corresponds to a print message
(print the string “Pi” and the PID of the process).
Check the precedence are respected by inserting
the system call sleep in the various branches of the program.
 */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>
int main(){
    pid_t pid_in, pid_out;
    printf("P1, PID=%d\n", getpid());

    //memorize pid and then wait for it
    //child
    pid_out = fork();
    if(!pid_out){
        sleep(5);
        printf("P3, PID=%d\n", getpid());

        sleep(5);
        //rhombus
        pid_in=fork();
        if(!pid_in){
            sleep(2);
            printf("P6, PID=%d\n", getpid());
            exit(0);
        }
        else{
            sleep(2);
            //print nothing because it says |
            while(wait((int*)0)!=pid_in);
        }
        sleep(2);
        printf("P8, PID=%d\n", getpid());
        exit(0);
    }
    else{
        printf("P2, PID=%d\n", getpid());

        wait((int*)0);


        //rhombus

        if(!fork()){
            sleep(5);
            printf("P5, PID=%d\n", getpid());
            exit(0);
        }
        else{
            printf("P4, PID=%d\n", getpid());
            //print nothing because it says |
            wait((int*)0);
        }
        sleep(2);
        wait((int*)0);
        printf("P7, PID=%d\n", getpid());
        
        sleep(2);
        //prevention to be su it finished all above processes
        wait((int*)0); 
        
        printf("P9, PID=%d\n", getpid());
    }


    return 0;
}




//left it's similar
