#include<stdio.h>
#include<dirent.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<string.h>
#include <malloc.h>
#include<unistd.h>

#define N  256+1

void dir_visit(DIR *, char [N]);

int main(int argc, char *argv[])
{
	DIR *dp=NULL;
	char path[N];
	struct stat stat_buf;

	sscanf(argv[1], "%s", path);
	stat(path,&stat_buf);

	// check if the fst char it's a . and the
	// getcwd and rewrite the relative path as absolute path
	if(!strcmp(path, ".")  ){
		//if it's a relative pass it transform it to abs path
		if(strlen(path)<2 || (path[1]=='/' || path[1]=='.' ))
		    getcwd(path, sizeof(path));
    	}
    	//if setted to path dir it changes to abs path
    	else if(!strcmp(path, "..") )
    	{
    		chddir(path);
    		getcwd(path, sizeof(path));
    	}
	//we assumned that the dir is not
	//check is it's a dir
	//return zero on success
	//return -1 if not success, but use <0 for error
	if(stat(path, &stat_buf)<0)
	{
		perror("Error : impossible retrive info about file\n");
		return -1;
	}
	//receving info about the file
	//we can check if it is a dir or not
	else if(!S_ISDIR(stat_buf.st_mode) )
	{
		perror("Error: path does not correspond to  directory path\n");
		return -2;
	}

	//OPEN DIR from CLI
	dp = opendir(path);

	if( dp==NULL)
	{
		perror("Directory could not be open\n");

		return -1;
	}

	//READ DIR
	dir_visit(dp, path);

	//CLOSE DIR
	closedir(dp);

	return 0;
}


/*
 * Args: dp the directory to be visited, the dir must differs from "." or ".."
*/
void dir_visit(DIR *dp, char prev_path[N])
{
    char full_path[N]="";
	struct dirent *file=NULL;
	struct stat stat_file;
	DIR *d_tmp;

	if(dp==NULL)
		return;

	//loop inside dir
	while( (file = readdir(dp)) !=NULL)
	{
		//memorize the stat: cause if it's a link it's better to redirect to the proper dir???
        sprintf(full_path, "%s/%s", prev_path,file->d_name);
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        //STAT WORK ON PATH AND NAME OF STRING
        if(stat(full_path, &stat_file)<0)
        {
            perror("Error : impossible retrive info about file\n");
            break;
        }
		// if dir tree visit
		// explicitly handle the  the dir .. to avoid endless recursions

            if(strcmp(file->d_name, ".") && strcmp(file->d_name, ".."))
            {


                if(S_ISDIR(stat_file.st_mode))
                {
                    //IN-ORDER-VISIT
                    //print dir
        //            printf("\nDirectory path:\t%s", prev_path);
                    printf("\nDir path:\t%s", full_path);
                    fflush( stdout );

                    //create the path of this dir
                    //visit the dir
                    d_tmp = opendir(full_path);
                    dir_visit(d_tmp, full_path);
                    closedir(d_tmp);
                }
                else
                {
                    printf("\nFile path:\t%s", full_path);
                    fflush( stdout );
                }

            }
		}

//	}
}
