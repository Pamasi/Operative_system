#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>

#define N 256+1

//PROTOTIPE
void cpy_dir(DIR *, DIR *, char [N], char [N]);
int  relToAbs_path( char *path);
int err_check(struct stat *,  char path[N] );





int main(int argc, char *argv[])
{
    char path1[N], path2[N];
    struct stat s_file1, s_file2;
    DIR *dsrc=NULL, *ddst=NULL;

    if(!strcmp(argv[1], "")  || !strcmp(argv[2], "") )
    {
        printf("Please valid arguments from command line\n");
        return -1;
    }


    //READ  NAME FILE FROM CLI
    sprintf(path1, "%s", argv[1]);
    sprintf(path2, "%s", argv[2]);

    if(relToAbs_path(path1)<0 ||relToAbs_path(path2)<0)
        return -5;


    //ERROR CHECK
    if( err_check(&s_file1, path1)<0 || err_check(&s_file2, path2)<0 )
        return -1;


    dsrc = opendir(path1);
    ddst = opendir(path2);

    //CPY DIR
    cpy_dir(dsrc, ddst, path1, path2);

    return 0;
}





int err_check(struct stat *s_file, char path[N] )
{
    int res;

    //ERROR CHECK
    if(stat(path, s_file)<0 )
    {
        perror("Invalid file\n");
        res = -2;
    }

    if(!S_ISDIR(s_file->st_mode))
    {
        perror("Please, insert a directory\n");
        res = -3;
    }
    else
    {
        res = 1;
    }

    return res;

}
int relToAbs_path(char *path)
{
    int res;

    if(path==NULL )
        res= -1;
    else if(!strcmp(path, ".") || !strcmp(path, "./")  ){
        //if it's a relative pass it transform it to abs path
        getcwd(path, sizeof(path));
        res = 1;
    }
        //if setted to path dir it changes to abs path
    else if(!strcmp(path, "..") )
    {
        chdir(path);
        if(getcwd(path, sizeof(path))==NULL)
            res=-1;
        else
            res = 2;
    }
        // if it's abs
    else
        res=3;

    return res;


}
/*
 * Args:src  directoriy whose content must be copied
 * 	dst directory where cpy src dir
 * 	in order to cpy all src content into dst , the dir must differs from "." or ".."
 * 	path1 path of src dir
 * 	path2 path of dst dir
*/
void cpy_dir(DIR *src, DIR *dst, char path1[N], char path2[N])
{
    int fd_src=-1, fd_dst=-1, n_elem=-1;
    char full_path1[N]="", full_path2[N]="", tmp_buf[N];
    struct dirent *file=NULL;
    struct stat stat_file;
    DIR *src_next=NULL, *dst_next=NULL;

    if(src== NULL || dst == NULL)
        return;

    //loop inside dir
    while( (file = readdir(src)) !=NULL)
    {
        //memorize the stat: cause if it's a link it's better to redirect to the proper dir???
        sprintf(full_path1, "%s/%s", path1,file->d_name);
        sprintf(full_path2, "%s/%s",path2, file->d_name);


        //STAT WORK ON PATH AND NAME OF STRING
        if(stat(full_path1, &stat_file)<0)
        {
            perror("Error : impossible retrive info about file\n");
            break;
        }
        // if dir tree visit
        // explicitly handle the  the dir .. to avoid endless recursions
        if(strcmp(file->d_name, ".") && strcmp(file->d_name, ".."))
        {
            if(S_ISDIR(stat_file.st_mode))
            {
                //IN-ORDER-VISIT
                //print dir
                printf("\nDir path:\t%s", full_path1);
                fflush( stdout );
                //STRATEGY:
                //the second path full to cpy
                //make dir
                mkdir(full_path2, 0777);

                //create the path of this dir
                //visit the dir
                src_next = opendir(full_path1);
                dst_next = opendir(full_path2);
                cpy_dir(src_next, dst_next, full_path1, full_path2);
                closedir(src_next);
                closedir(dst_next);

            }
            else
            {
                printf("\nFile path:\t%s", full_path1);
                fflush( stdout );
                //CREATE FILE IN POSIX STD
                fd_src = open(full_path1, O_RDONLY);
                fd_dst = open(full_path2, O_WRONLY | O_CREAT);

                //CPY FILE CONTENT
                while((n_elem=read(fd_src, tmp_buf, sizeof(tmp_buf)) !=-1))
                {
                    if(write(fd_dst, tmp_buf, n_elem) ==-1)
                    {
                        fprintf(stderr,"Problem copy file %s\n", file->d_name);
                        break;

                    }


                }

                close(fd_src);
                close(fd_dst);
            }

        }



    }
}
