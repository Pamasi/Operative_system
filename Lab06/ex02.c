#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<stdbool.h>



int main(int argc, char **argv){
    int fd_pipe[2], pid;
    char buf[]="working";

    if(pipe(fd_pipe)==1){
        fprintf(stderr, "Error creating pipe\n");
        exit(EXIT_FAILURE);
    }

    pid = fork();

    if(pid==-1){
        fprintf(stderr, "Fork error\n");
        exit(EXIT_FAILURE);
    }
    else if(pid==0){
        // child -> write
        close(fd_pipe[0]);
        while(true){
            if( write(fd_pipe[1],buf, sizeof(buf) )<= 0){
                fprintf(stderr, "Wrintg pipe error\n");
                exit(EXIT_FAILURE);           
            }
            printf("I'm the child (PID=%d)\n", getpid());
        }

    }
    else{
        //parent
        close(fd_pipe[1]);
        while(true){
            if( read(fd_pipe[0],buf, sizeof(buf) )<= 0){
                fprintf(stderr, "Reading  pipe error\n");
                exit(EXIT_FAILURE);           
            }
            printf("I'm the father (PID=%d)\n", getpid());
            // we use getpid because pid
            // returned by fork is the children pid
        }


    }

    return 0;
}