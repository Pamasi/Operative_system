#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<unistd.h>
#include<signal.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<assert.h>
#include<stdbool.h>

#define N 20
#define DEBUG 0
#define N_PIPE 2

static const unsigned int nprocess = 2;

static void s_handler(int sig);
static void consumer(unsigned int prod_pid,  int *fd_pipe);
static void producer(int *fd_pipe );


int main( int argc, char **argv){
    int pid, prod_pid;
    int fd[N_PIPE];

    setbuf(stdin,0);
    setbuf(stdout,0);

    // declaration of signal handler
    if( signal(SIGUSR1, s_handler) == SIG_ERR){
        fprintf(stderr, "Error assigning signal handler\n");
        return(1);
    }

    // init pipe
    if(pipe(fd)==- 1){
        fprintf(stderr, "Error init pipe\n");
        exit(EXIT_FAILURE);
    }

    // REMEMBER: fork
    //          return 0 if child, 
    //                 childPid if parent
    //handle fst child (producer)
    pid =fork();

    if(pid==-1){
        fprintf(stderr, "Fork ERROR\n");
        exit(EXIT_FAILURE);
    }
    if(pid>0){
        // create consumer
        prod_pid = pid;
        pid = fork();

        if(!pid){
            consumer(prod_pid, fd);
            exit(EXIT_SUCCESS);
        }
        else{
            close(fd[0]);
            close(fd[1]);
            for(int i=nprocess; i>0; --i) wait(NULL);
        }
    }
    //producer
    else{
        producer(fd);
        exit(EXIT_SUCCESS);

    }
  
#if DEBUG==1 
printf("Exit  pid=%d\n", getpid()); 
#endif
}

/*FUNCTIONS*/
static void s_handler(int sig){
    switch (sig)
    {
        case SIGUSR1:
#if DEBUG==1
    printf("\nProcess pid=%d caught SIGUSR1\n", getpid());
#endif
        break;
        default:
            printf("Wrong signal caught\n");
            exit(EXIT_FAILURE);
    }

}

static void consumer(unsigned int prod_pid, int *fd_pipe){
    unsigned int  pid;
    int   end=1;
    char buf[N];

    // 0-> read, 1->write
    pid = getpid();
    #if DEBUG==1
    printf("Consumer pid =%d\n", pid);
    #endif
    #if DEBUG==1
    printf("fd_pipe[0] =%d\n", fd_pipe[0]);
    printf("fd_pipe[1] =%d\n\n", fd_pipe[1]);
    #endif

    close(fd_pipe[1]);


    do{ // start work after receiving signal from producer
        // pause();    
        if( read(fd_pipe[0], buf, sizeof(buf)) > 0 ){

            end = strcmp(buf, "end");
            if(end){
                for(int i =0; i< N; ++i )
                    buf[i] = toupper(buf[i]);

                printf("modified string:%s\n", buf);
                fflush(stdout);
            }   
        }

    }while(end);

    close(fd_pipe[0]);

}

static void producer( int *fd_pipe){
    unsigned int cons_pid;
    char buf[N];
    setbuf(stdout, 0);

    #if DEBUG==1
    printf("Producer pid=%d\n", getpid());
    #endif
    #if DEBUG==1
    printf("fd_pipe[0] =%d\n", fd_pipe[0]);
    printf("fd_pipe[1] =%d\n\n", fd_pipe[1]);
    #endif

    close(fd_pipe[0]);

    do{
        printf("Insert a string\n");
        scanf("%s", buf);
        
        if( write(fd_pipe[1], buf, sizeof(buf)) < strlen(buf)){
            fprintf(stderr, "Writing  producer ERROR\n");
            exit(EXIT_FAILURE);
        }  
  
    }while(strcmp(buf, "end"));
    close(fd_pipe[1]);
}