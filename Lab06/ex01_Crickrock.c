#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include <string.h>
#include <sys/wait.h>

#define LEN 50

void sigUsrHandler(int sig);
char* toUpperStr(char *str, int maxlen);

int main() {
  pid_t ch1pid, ch2pid;
  int fd[2];

  if(pipe(fd) == -1) {
    fprintf(stderr, "ERRORE: impossibile creare la pipe.\n");
    return EXIT_FAILURE;
  }
  
  signal(SIGUSR1, sigUsrHandler);

  if((ch1pid = fork())) {
    if((ch2pid = fork())) {   // Padre
      close(fd[0]);
      write(fd[1], &ch1pid, sizeof(int));
      close(fd[1]);
      
      wait((int*) 0);
      wait((int*) 0);
      
    } else {   // Figlio 2 (consumatore)
      char str2[LEN];
      
      close(fd[1]);
      read(fd[0], &ch1pid, sizeof(int));

      while(1) {
	read(fd[0], &str2, LEN);
	printf("%s\n", toUpperStr(str2, LEN));
	sleep(1);
        kill(ch1pid, SIGUSR1);
      }
    }
    
  } else {  // Figlio 1 (produttore)
    char str1[LEN];
    
    close(fd[0]);
    
    while(strcmp("end\n", fgets(str1, LEN, stdin))) {
      write(fd[1], &str1, strlen(str1)+1);
      pause();
    }
    kill(ch2pid, SIGKILL);
    close(fd[1]);
  }
  
  return EXIT_SUCCESS;
}

void sigUsrHandler(int sig) {}

char* toUpperStr(char *str, int maxlen) {
  int len = strlen(str), i;
  for(i = 0; i < len && i < maxlen; i++)
    str[i] = toupper(str[i]);
  return str;
}
