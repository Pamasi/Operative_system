#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<math.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/types.h>
#include<stdbool.h>
#define NT 4
#define NBUFF 3
#define MONO 3

#define VERIFY 0

typedef struct{
    int *down, *up; 
    int DIM_D, DIM_UP;
} file_t;

static  sem_t *S;
static  float polynomial[MONO]={0.0,0.0,0.0};
static int n_mono=-1;
static  bool end=false;
static file_t *arg;
static int fd;
static void *compute(void *arg);
static void *update(void *arg);


int main(int argc, char **argv){

    pthread_t t[NT];
    
    //by name of the file we means the path
    if(argc == 1){
        fprintf(stderr, "Invalid num params");
        exit(EXIT_FAILURE);
    }

#if VERIFY==1
    int fd=open(argv[1], O_CREAT | O_WRONLY, S_IRWXU | S_IRWXG);
    float val[]={1.0,2.0,3.4,4.6,4.8,6.2};
    int exp[]={1,2,5};
    
    //... add if you want


#endif
    S = (sem_t*) malloc(NT*sizeof(sem_t));
    sem_init(S, 0, 1);
    for(int i =0; i<NT; ++i)  
        sem_init(S+i, 0, 0);


    

    // init arg
    arg = (file_t*) malloc(NT*sizeof(file_t));
    int down0[]={0};     arg[0].down=down0;  
    int down1[]={1};     arg[1].down=down1;
    int down2[]={2};     arg[2].down=down2;
    int down3[]={0,1,2}; arg[3].down=down3;   
    int up0[]={1,3};     arg[0].up=up0;         
    int up1[]={2,3};     arg[1].up=up1;
    int up2[]={3};       arg[2].up=up2;
    int up3[]={0};       arg[3].up=up3;
    arg[0].DIM_D=1;      arg[0].DIM_UP=2;
    arg[1].DIM_D=1;      arg[1].DIM_UP=2;
    arg[2].DIM_D=1;      arg[2].DIM_UP=1;
    arg[3].DIM_D=3;      arg[3].DIM_UP=1;   
       

    fd = open((char*)arg, O_RDONLY);

    // init thread
    for(int i=0; i<3; ++i)
        pthread_create(t+i, NULL, compute, (void*) &arg[i]);

    pthread_create(t+3, NULL, update, (void*)&arg[3]);

    for(int i =0; i<NT; pthread_join(t[i++],NULL) );

    for(int i =0; i<NT; sem_destroy(&S[i++])) ;

    free(S);
    close(fd);

    return 0;
}


static void *compute(void *arg){
    int  i, x, n_int;
    float n_real[NBUFF];

    while(true){
        for(i =0; i< ((file_t*)arg)->DIM_D; i++)
            sem_wait(S + ((file_t*)arg)->down[i]  );

        if(!end){
            // cs
            // remember that sizeof return the byte size
            // we reached the end of the file
            
            if( read(fd,&n_real, sizeof(n_real))<=0 && 
                read(fd, &n_int, sizeof(n_int)) <=0 ) {
                
                n_mono==0?(end=true): (polynomial[n_mono]=0.0);
                   
            }
            else{
                // compute  monomial calculus
                for( i=0, x=1; i<n_int; i++)
                    x*=n_real[1];   //x = 1*n_real*n_real*...*n_real
                x*=n_real[0]; //c*x^n

                // update the vector
                polynomial[n_mono++]=x;
            }
        }

        for(i = 0; i< ((file_t*)arg)->DIM_UP; i++)
            sem_post(S + ((file_t*)arg)->up[i]  );

        if(end) break;
    }

    pthread_exit(NULL);
}

static void *update(void *arg){
    int  i;
    float sum=0.0;
  
    while(true){
        
        for(i =0; i< ((file_t*)arg)->DIM_D; i++)
            sem_wait(S +((file_t*)arg)->down[i]  );

        if(end) break;
        // cs
        for(i = 0; i< MONO; i++){
            sum += polynomial[i];
            // resolve problem line not multiple 3
            polynomial[i]=0.0;
        }
        printf("sum=%f\n", sum);
        for(i = 0; i< ((file_t*)arg)->DIM_UP; i++)
            sem_post(S + ((file_t*)arg)->up[i]  );
    }

    pthread_exit(NULL);

}