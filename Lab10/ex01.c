/* WARNING
    DO NOT USE  posix file: more 
    complicated to handle (fscanf etc..)
*/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<pthread.h>
#include<semaphore.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/types.h>
#include<stdbool.h>

#define NT 3
#define NL 100

static  sem_t *S;
static  char line[NL];
static  bool end=false;

static void *readfile(void *arg);
static void *convert();
static void *update(void *arg);

int main(int argc, char **argv){
    pthread_t t[NT];
    FILE *fout,  *fin;

    //by name of the file we means the path
    if(argc == 2){
        fprintf(stderr, "Invalid num params");
        exit(EXIT_FAILURE);
    }

    fin = fopen(argv[1],"r");
    fout =fopen(argv[2], "w+");

    if( fout== NULL|| fin == NULL){
        fprintf(stderr, "File not found");
        exit(EXIT_FAILURE);
    }


    S = (sem_t*) malloc(NT*sizeof(sem_t));
    sem_init(S,0, 1);
    for(int i =1; i<NT; sem_init(&S[i++],0, 0) );

    pthread_create(t, NULL, readfile, (void*) fin);
    pthread_create(t+1, NULL, convert, NULL );
    pthread_create(t+2, NULL, update, (void*) fout);

    for(int i =0; i<NT; pthread_join(t[i++],NULL) );
    for(int i =0; i<NT; sem_destroy(&S[i++]) );

    fclose(fin);
    fclose(fout);
    free(S);

    return 0;
}


static void *readfile(void *arg){
    while(true){
        if(end) break;

        sem_wait(S);

        if(fgets(line, NL, (FILE*)arg)==NULL)
            end =true;

        sem_post(S+1);

    }

    pthread_exit(NULL);
}
static void *convert(){
    int len, odd=0;
    char tmp;
   

    while(true){
        if(end) {
            sem_post(S+2);
            break;
        }
        sem_wait(S+1);

        len = strlen(line);
       
        if(line[len-1] == '\n') --len;
        if(len%2!=0) odd=1;

        for(int i =0; i<(len/2+odd); i++ ){
            tmp = line[i];

            if(islower(tmp))
                tmp =toupper(tmp);

            if(islower(line[len-i-1]))
                line[len-i-1] = toupper(line[len-i-1]);

            line[i] = line[len-i-1] ;
            line[len-i-1] = tmp;
        }

        sem_post(S+2);
    }


    pthread_exit(NULL);
}


static void *update(void *arg){
    while(true){
        sem_wait(S+2);
        if(end) break;

        if(fputs(line, (FILE*)arg)<=0){
            // to permit to terminate the other thread
            fprintf(stderr, "ERROR: file writing\n");
            end = true;
            sem_post(S);
            break;
        }

        sem_post(S);
    }
  
    pthread_exit(NULL);
}