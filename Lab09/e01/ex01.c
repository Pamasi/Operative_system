#include<stdio.h>
#include<stdlib.h>


static int ticketNumber=0;
static int turnNumber=0;


static int atomicIncrement (int *var) {
  int tmp = *var;
  *var = tmp + 1;
  return (tmp);
}


void init(int *var){
    /* increment n* ticket and assign the relative turn number*/
     *var = atomicIncrement(&ticketNumber);

}

/*lock block other resources that want ot accesss*/
int  lock(int *var){

    int res;
    
    turnNumber == *var ? ( res = 1): (res = 0);

    return res;
}


/*unlock unblock all resources that want ot accesss*/
unsigned int unlock(int *var){
    int res;

    if(turnNumber == *var){
        res = 1;

        /*reset if it has done all the tickets*/
        if(ticketNumber == atomicIncrement(&turnNumber) )
            ticketNumber = turnNumber = 0;
    }
    else{
        res = 0;
    }

    return res;
}