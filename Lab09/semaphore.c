#include "semaphore.h"
// Mutual Exclusion semaphore
semaphore me;

semaphore init(int k ) {
  int *s;
  //create pipe for semaphore
  s = (semaphore)calloc (2,sizeof(int));
  pipe (s);

  if (k){
      if( write(s[1], &k, sizeof(int))<=0){
        fprintf(stderr, "ERROR : init\n");
        exit(1);
      }
  }
  
  return s;
}

void WAIT (semaphore s) {
  int junk;

  if (read(s[0], &junk, 1) <=0) {
    fprintf(stderr, "ERROR : wait\n");
    exit(1);
  }
}

void SIGNAL(semaphore s) {
  if (write(s[1], "x", 1) <=0) {
    fprintf(stderr, "ERROR : signal\n");
    exit(1);
  }
}

void process (int id) {
  int i;

  for(i=0; i<3; i++) {
    sleep(1);  
    WAIT(me); 
    printf("  Entering critical region of process %d\n", id);
    sleep(2);
    printf("    Exiting critical region of process %d\n", id);
    SIGNAL(me);
  }
  exit (0);
}

void destroy(semaphore S){
    free(S);
}
