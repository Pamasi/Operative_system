#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>

static const int n_t = 9;
enum id{ A=0,B,C,D,E,F,G,H,I};

typedef struct{
    int *down, *up;
    int DIM_D, DIM_UP;
    char letter;
} file_t;

// semaphore declaration
static sem_t **S;

static void *subprocess(void *arg);

int main(int argc, char **argv){
    int a_down[]={0},   a_up[]={0,1,2};
    int b_down[]={0},   b_up[]={6};
    int c_down[]={1},   c_up[]={3,4};
    int d_down[]={2},   d_up[]={7};
    int e_down[]={3},   e_up[]={5};
    int f_down[]={4},   f_up[]={1};
    int g_down[]={5,1}, g_up[]={1};
    int h_down[]={7},   h_up[]={6};
    int i_down[]={6,6,1};
    int i;
    char offset;
    pthread_t *tid = NULL;
    file_t *arg = NULL;
    

    // init sem
    S = (sem_t **)malloc(n_t*sizeof(sem_t));
    for(i = 0; i< n_t; i++){
        S[i] =(sem_t*) malloc(sizeof(sem_t));
        i==0?sem_init(S[i], 0, 1):sem_init(S[i], 0, 0);
    }

    while(true){
        // generation of thread 
        tid = (pthread_t*) malloc( n_t*sizeof(pthread_t));

        // generation of params thread
        arg = (file_t*) malloc(n_t*sizeof(file_t));

        // fill params thread
        arg[A].down = a_down;  arg[A].DIM_D=1;  arg[A].up = a_up; arg[A].DIM_UP=3;
        arg[B].down = b_down;  arg[B].DIM_D=1;  arg[B].up = b_up; arg[B].DIM_UP=1; 
        arg[C].down = c_down;  arg[C].DIM_D=1;  arg[C].up = c_up; arg[C].DIM_UP=2; 
        arg[D].down = d_down;  arg[D].DIM_D=1;  arg[D].up = d_up; arg[D].DIM_UP=1; 
        arg[E].down = e_down;  arg[E].DIM_D=1;  arg[E].up = e_up; arg[E].DIM_UP=1; 
        arg[F].down = f_down;  arg[F].DIM_D=1;  arg[F].up = f_up; arg[F].DIM_UP=1; 
        arg[G].down = g_down;  arg[G].DIM_D=2;  arg[G].up = g_up; arg[G].DIM_UP=1; 
        arg[H].down = h_down;  arg[H].DIM_D=1;  arg[H].up = h_up; arg[H].DIM_UP=1; 
        arg[I].down = i_down;  arg[I].DIM_D=3;  arg[I].up = NULL; arg[I].DIM_UP=0; 

        for(i = 0, offset = 'A'-1; i<n_t; i++)
            arg[i].letter=++offset;
 

        // create
        for(i = 0; i<n_t; i++)
            pthread_create(tid+i, NULL, subprocess, arg+i);

        // destroy
        for(i = 0; i<n_t; i++)
            pthread_join(tid[i], NULL);

        free(tid); free(arg);
    }

    return 0;
}

static void *subprocess(void *arg){
    int *d =  ((file_t *)arg ) ->down;
    int *u =  ((file_t *)arg ) ->up;

    for(unsigned long i = 0; i<((file_t *)arg ) ->DIM_D; i++)
        sem_wait(S[ d[i] ]);
    // cs
    printf("%c\n", ((file_t *)arg)->letter);

    for(int i = 0; i<((file_t *)arg ) ->DIM_UP; i++)
        sem_post(S[ u[i] ]);
    
    pthread_exit(NULL);
}
