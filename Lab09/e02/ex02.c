//        A  <--------
//     /  |  \       ^
//    /   |   \      |
//   B    C    D     |
//   |   / \   |     |
//   |  /   \  |     |
//   | E    F  |     |
//   |  \   /  |     |
//   |   \ /   |     |
//   |    G    H     |
//    \   |   /      |
//     \  |  /       |
//        I  ---------
// Exercise 02
// Each task(vertex) corresponds to a process.
// Each process is (re-)created and destroyed at each iteration of the principal cycle.
// Semaphores are implemented by means of pipes.
// CHILD PROCESS DON'T SHARE MEMORY WHEN IT'S MODIFIED BY THEMSELVES
// since process exit after finish you can reuse semaphore

#include "semaphore.h"
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<stdbool.h>

static semaphore *S;
static const int n_sem = 5;
static int process(int pid, const int NPR);
static void subprocess(char *down, char *up, char letter);

int main(int argc, char **argv){
    
    int pid, NPR=9;

    
    while(true){
        pid = fork();

        if( pid == -1){
            fprintf(stderr, "ERROR fork\n");
            exit(EXIT_FAILURE);
        }
        else if(!pid){
                process(pid, NPR-1);
        }
        else{
            // wait child termination: use to manage error case
            wait(&pid);
        }
    }
}

static void subprocess(char *down, char *up, char letter){

    for(char* p = down; *p!='\n'; p++)
        WAIT(S[*p -'0']);
    // css
    printf("%c\n", letter);

    for(char* p = down; *p!='\n'; p++)
        SIGNAL(S[*p-'0']);
}

static int process(int pid, const int NPR){

    int i;
    char name= 'A';

    setbuf(stdin,0);
    setbuf(stdout,0);

    S[0]=init(1);
    for( i = 1; i<n_sem; S[i++]=init(0));
    
  
    subprocess("0", "012", 'A');
    // correct after works it dies
    /* create process */
    i = 0;


    while(i < NPR){
        if(pid){
            pid=fork();
            if(pid == -1){
                fprintf(stderr, "ERROR fork\n");
                exit(EXIT_FAILURE);
            }
            else if(pid){
                name++;
            }
        }

    }

    switch(name){
        case 'B':
            subprocess("0", "6", name);
            break;
        case 'C':
            subprocess("1", "34", name);
            break;
        case 'E':
            subprocess("3", "5", name);
            break;
        case  'F':
            subprocess("4", "1", name);
            break;
        case  'G':
            subprocess("51", "1", name);
            break;
        case 'D':
            subprocess("2", "7", name);
            break;
        case 'H':
            subprocess("7", "6", name);
            break;
        case 'I':
            subprocess("661", "\n", 'I');
            for(i = 0; i < n_sem; destroy(S[i++]) );
        
    }
    
    exit(EXIT_SUCCESS);
}

