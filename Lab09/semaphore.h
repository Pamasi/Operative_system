/*
 * Illustrates the implementation of semaphores by means of
 * system call pipe
 * 2 Parameters on command line:
 * - # of process
 * - # of process in SC
 *
*/
#ifndef SEMAPHORE
#define SEMAPHORE_H

#include <stdio.h>		
#include <stdlib.h>
#include <unistd.h>

// Mutual Exclusion semaphore
typedef int * semaphore;

semaphore init();
void destroy(semaphore S);
void WAIT (semaphore s) ;
void SIGNAL(semaphore s);

#endif