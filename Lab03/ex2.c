#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <stdbool.h>

int main(int argc, char *argv[]){
    int n, t, ppid, tmp;
    bool check=false;
    if(argc!=3)
        return -1;

    n = atoi(argv[1]);
    t = atoi(argv[2]);

    printf("INIT\nPID=%d\tPPID=%d\n\n\n", getpid(), getppid());

    for (int i = 0; i <n ; ++i) {
        // it work because:
        // 1)for parent process (corrisponding to
        //   the program) it's obvious (see the 2 fork)
        // 2)for the children
        //   at first time one child is created
        //   it can't reach any further fork
        //   so it continue the loop
        //   and in the first if it fork
        //   became a father
        //   the same schema it's repeated for
        //   the other children

        if(fork()){
            //parent
            if(fork()){
                //Parent
                printf("killed parent pid=%d\n", getppid());
                exit(0);
            }
            //child
            else{
                printf("PID=%d; PPID=%d;", getpid(), getppid());
            }
        }
        //child
        else{
            printf("PID=%d; PPID=%d;",getpid(), getppid());
        }
    }

    printf("PID=%d is terminated\n", getpid());
    sleep(t);
    return 0;
}
