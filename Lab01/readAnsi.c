#include<stdio.h>
#include<stdlib.h>

#define N 128

int main(int argc, char *argv[])
{
	FILE *fin=NULL, *fout=NULL;
	char buffer[N];
	int n_elem=-1;

	//DEBUGGING
	printf("input name = %s\toutput name= %s\n", argv[1], argv[2]);

	//READING
	fin = fopen(argv[1], "r");
	fout= fopen(argv[2], "w+");

	//READ DATA
	while( (  n_elem=fread(buffer,1,  sizeof(buffer), fin)  ) !=0 )
	{

		//WRITE DATA
		//parameter: destionation location, size of each iteam data, size of read data itemS (it's plural), src stream  
		if(fwrite(buffer,1, n_elem*sizeof(char), fout) !=n_elem)
	 	{
			fprintf(stderr, "Error buffering\n");
		}

	}
	
	fclose(fin);
	fclose(fout);
	return 0;

}
