#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

#define N 128+1

int main(int argc, char *argv[])
{
	int fd1=-1, fd2=-1, n_elem=-1; // fd stands for file descriptor
	char buffer[N];

	// OPEN FILE
	fd1 = open(argv[1], O_RDONLY);
	fd2 = open(argv[2], O_WRONLY | O_CREAT);



	fprintf(stdout, "File descriptor\tn°\n%d\t%d\n", fd1, fd2);
	
	if(fd1==-1)
	{
		fprintf(stderr, "Problem finding file output\tERROR NUM=%d", fd1);
		return -1;
	}

	if(fd2==-1)
	{
		fprintf(stderr, "Problem finding or creating file output\tERROR NUM==%d", fd2);
		return -2;
	}

	//READ FILE
	while( (n_elem=read(fd1, buffer, N*sizeof(char))  )>0 ) 
	{
		if( write(fd2, buffer, n_elem*sizeof(char)==-1) )
		{
			fprintf(stderr, "ERROR writing file\n");
		}	
	}

	return 0;


}
