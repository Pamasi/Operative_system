#!/bin/bash

if [ $# -ne 2]; then
	echo "Wrong args format"
	exit 1
fi

let flag=5


while [ $flag -gt 0 ]; do
	res=$(ps -el | cut -d ' ' -f 1,3-8,10-12 | grep -E "Z\s(/.+/)+$1")

	if [ -z $res ]; then
		let flag=5
	else
		let flag--
	fi
 	

	sleep $2
done

kill -SIGKILL $pid
