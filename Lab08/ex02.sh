#!/bin/bash
# check if file exits 
#to use var -> myvar = myval
# to cal var   $myvar

# NO SPACE BETWEEN var and assignment
res=$(find . -name $1 | wc -l)

#SPACE BETWEEN echo and ""
#echo "res value=$res"

if [ $res -lt 1 ]; then
	echo "ERROR: file not found\n"
	exit 1
elif [ ! -f $1 ]; then
	echo "ERROR: incorrect type of file\n"
	exit 2 

fi

let max=0

#read line by line
while read line; do
	#iterate in line as vector
	tmp=0
	for i in $line; do
		let tmp=tmp+1
		
	done
#	echo "length line=$tmp\n"	
	if [ $tmp -ge $max ]; then
		max=$tmp
		out=$line
	fi	
done < $1

echo -e "max\tvalue\tstring\n\t$max\t$out\n"
