#!/bin/bash
if [ -z $1 ]; then
	echo "Invalid params"
	exit 1
fi

declare -A token

for  word in $(cat $1); do
	# search if key exit (not empty)
	# works 'cause the fst is empty
	if [ -z token[$words] ]; then
		let token[$word]++ 
	else 
		let token[$word]++
	fi
done

for key in ${!token[*]}; do
	echo -e "$key ${token[$key]}"
done
