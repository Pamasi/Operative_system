#!/bin/bash
# we assume that usr on put the name  andd not the path
# check if it is arg1 is empty
if [ $# -lt 3 ]; then
	echo "ERROR: invalid srgs num\n"
	exit 1
fi

let res=$(find . -name $1 -type d| wc -l)

echo "res=$res"

echo "Insert 0\\1"
read mode

echo "mode is=$mode"

if [ $res -lt 1 ]; then
	echo "ERROR: directory not found\n"
	exit 2
fi

# create file 
# search function

### THE $[expr] create a  in###
if [ "$mode" = "0" ]; then
	echo -e "fst if entered\n"
	find -regextype posix-extended -regex "\./(.+/)*.*\.c" -exec grep -E "$2\s*\(.*(,.+)*\)" -H -n \{} \; >>$3

elif [ "$mode" = "1" ]; then
	echo "./$1/(.+/)*.*\.c"
	tmp=$(find -regextype posix-extended -regex "\./$1/(.+/)*.*\.c")

	echo "tmp is=$tmp"
	if [ -z "$tmp" ]; then
		echo -e "ERROR find\n"
		exit 3
	fi

	for i in tmp; do
		grep -E "$2\s*\(.*(,.+)*\)" -H -n $tmp >>$3 
	done 
else 
	echo "Input error"
	exit 3	
fi

