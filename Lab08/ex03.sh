#!/bin/bash

if [ -z $1 ]; then 
	echo -e " Wrong num of params\n"
	exit 1
fi

echo -e "Search in all directory?[0\\1]"
# pattern
pattern="./$1/(.+/)*.+"
pattern="./$1/(.+/)*.+"

# create tmp file
touch tmp
# transform to lower case
src=$(find -regextype posix-extended -regex $pattern  -type f,d) 

# to consider all the subdir (not required by the text)
src=$(find -regextype posix-extended -regex $pattern  -type f,d | sort -r )

echo "$src"


# we use quotes since it's list of element in order to transform it to one element
if [ -z src ]; then
	echo"No file or dir found\n"
	exit 2
fi

# using $var treat it as an array whose element are delimited by \n
# using var treat it as a string
for e in $src; do
	# retrieve the basename and change it lower
	baseOld=$(basename $e)
	baseNew=$(basename $e | tr '[:upper:]' '[:lower:]' )
	path=$(dirname $e)

	if [ "$baseOld" != "$baseNew" ]; then
		echo  "change $e to $path/${baseNew}"	
		# rename file
		mv $e $path/${baseNew}
	fi

done
