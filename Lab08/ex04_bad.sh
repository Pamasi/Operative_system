#!/bin/bash

if [ -z $1 ]; then
        echo "Invalid params"
        exit 1
fi

declare -a freq
declare -a token

#read a file
i=0
let flag=0
for word in $(cat $1); do
	let j=0
	
	while [ ${#token[*]} -gt 0 -a  $j -lt ${#token[*]}  ] && [ $flag -eq 0 ]; do
		if  [ "${token[$j]}" == "$word" ]; then
			let flag=1
		else
			let j++
		fi
	done

	if [ $flag -eq 1 ]; then
		let freq[$j]=freq[$j]+1
	else
		let freq[$i]=1
		token[$i]=$word
		let i++
	fi

	let flag=0
done


for i in  ${!freq[*]}; do
        echo -e "${token[$i]}\t${freq[$i]}\n"
done

