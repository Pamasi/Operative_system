#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>

/* struct*/
typedef  struct{
    int i;
    char *in;
} file_t;


/* remember to free*/
static char *strcat2(const char *src, char const*postfix);

/* SORT FUNCTION*/
static void BottomUpMergeSort(int *A, int N);
static  int min(int A, int B) ;
static void Merge(int *A, int *B, int l ,int  q, int r);

/* THREAD FUNCTION */
static void *sort_file( void *arg);

/* GLOBAL VAR*/
static int **f_store;
// dim of each matrix row
// or better use the return value from thread
static int *d_store;


int main(int argc, char **argv){

    int i, j, n, joined =-1;
    char *strA = NULL, *out = NULL, postfix[]="0.txt";
    file_t *arg = NULL;
    FILE * fout = NULL;
    pthread_t *tid = NULL;
    int *merge = NULL, *tmp = NULL, len ;



    if( argc!=4){
        fprintf(stderr, "Invalid number args\n");
        exit(EXIT_FAILURE);
    }
    /* str 2 int*/
    n  = atoi(argv[1]) ;
    /* point to specific arg*/
    strA = argv[2];
    out = argv[3];
    strcat(out, ".txt");

    /* generation of request threads*/
    tid = (pthread_t*) malloc (n*sizeof(pthread_t));

    /* generation of params thread*/
    arg = (file_t*) malloc(n*sizeof(file_t));

    /* generation matrix store*/
    f_store = (int**) malloc( n*sizeof(int*));
    d_store = (int*) malloc( n*sizeof(int*));

    /*each thread sort one file independentely*/
    for( i=0; i <n; ++i){
        postfix[0] =   i + 1 + '0';
        arg[i].in = strcat2(strA, postfix);
        arg[i].i=i;

        if(pthread_create(tid+i, NULL, sort_file, (void *)&arg[i] ) ){
            /* since all thread belongs to the same process
              exit the process cause the auto termination
              of ALL thread*/
            fprintf(stderr, "Thread creation ERROR\n");
            exit(EXIT_FAILURE);
        }



    }

    /* DO NOT USE IN THE PREV FOR OTHERWISE IT'S NOT PARALLEL PROGRAMMING!*/
    for(i =0, len=0; i<n; ++i){


        /* no more used: free when thread join*/
        /* the return value from thread determine success or failure
         * the last arg is the retgurn value from THAT thread*/
        if(!pthread_join(*(tid+i), NULL)){
            /* cpy to merge*/

            if(i) {
                free(merge);
            }
            merge = (int*) malloc( len + d_store[i]*sizeof(int) );

            for(j =0; j<len+d_store[i]; ++j){
                if( j<len)
                    merge[j] = tmp[j];
                else
                    merge[j] = f_store[i][j];

            }


            len+=d_store[i];
            /* realloc only when needed, so after the snd (real) merge*/
            if(i>0) free(tmp);
            tmp = (int*) malloc(len*sizeof(int));
            for (j= 0; j < len ; j++){
                tmp[j] = merge[j];
            }

        }

        free(arg[i].in);
    }
    free(arg);


    /* write on file*/
    fout = fopen(out, "w");
    if(fout == NULL){
        fprintf(stderr, "Error opening file\n");
        exit(EXIT_FAILURE);
    }
    BottomUpMergeSort(merge, len);
    for ( i = 0; i < len; i++)
        fprintf(fout, "%d\n", merge[i]);

    fclose(fout);



    for(i = 0; i<n ; ++i)
        free(f_store[i]);

    free(f_store);
    free(merge);
    free(tmp);
    free(d_store);


    return 0;
}




static char *strcat2(const char *src, const char *postfix){
    char *tmp;

    tmp = (char*)malloc(strlen(src) + strlen(postfix) );
    tmp = strcpy(tmp, src );
    strcat(tmp, postfix );

    return tmp;

}


/*sort */
static void BottomUpMergeSort(int *A, int N){
    int i, m, l=0, r=N-1;
    int *B = (int *)malloc(N*sizeof(int));
    if (B == NULL) {
        printf("Memory allocation error\n");
        exit(1);
    }

    for (m = 1; m <= r - l; m =(m + m) ) {
        for (i = l; i <= r-m ; i +=( m + m) )
            Merge(A, B, i, i+m-1, min(i+m+m-1,r));
    }

    free(B);
}

static  int min(int A, int B) {
    if (A < B)
        return A;
    else
        return B;
}
static void Merge(int *A, int *B, int l ,int  q, int r){
    int i, j, k;
    i = l;
    j = q+1;
    for(k = l; k <= r; k++)
        if (i > q)
            B[k] = A[j++];
        else if (j > r)
            B[k] = A[i++];
        else if (    A[i] <= A[j]  )
            B[k] = A[i++];
        else
            B[k] = A[j++];
    for ( k = l; k <= r; k++ )
        A[k] = B[k];

}


/* function handle file*/
static void  *sort_file( void *arg){

    int n, i,  *tmp_p;
    FILE *fin=NULL;

    fin = fopen( ( (file_t*)arg )->in, "r");


    if(fin == NULL ){
        fprintf(stderr, "Error opening file\n");
        exit(EXIT_FAILURE);
    }
    fscanf(fin, "%d", &n);

    /* use tmp_p for readibilty*/
    f_store[( (file_t*)arg)->i] = malloc(n*sizeof(int));
    d_store[( (file_t*)arg)->i] = n;
    tmp_p = f_store[( (file_t*)arg)->i];

    for ( i = 0; i < n; i++)
        fscanf(fin, "%d", tmp_p+i);

    BottomUpMergeSort(tmp_p, n);


    /* close file*/
    fclose(fin);


    pthread_exit((int*)0);
}