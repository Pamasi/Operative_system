#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>


/* struct*/
typedef struct{
    char *in;
    char *out;
} file_t;


/* remember to free*/
char *strcat2(const char *src, char const*postfix);

/* SORT FUNCTION*/
static void BottomUpMergeSort(int *A, int N);
static  int min(int A, int B) ;
static void Merge(int *A, int *B, int l ,int  q, int r);
static void *sort_file( void *arg);

/* THREAD FUNCTION */


int main(int argc, char **argv){

    int i, n, joined;
    char *strA, *strB, postfix[]="0.txt";
    file_t *arg;
    pthread_t *tid;



    if( argc!=4){
        fprintf(stderr, "Invalid number args\n");
        exit(EXIT_FAILURE);
    }
    /* str 2 int*/
    n  = atoi(argv[1]) ;
    /* point to specific arg*/
    strA = argv[2];
    strB = argv[3];

    /* generation of request threads*/
    tid = (pthread_t*) malloc (n*sizeof(pthread_t));

    /* generation of params thread*/
    arg = (file_t*) malloc(n*sizeof(file_t));



    /*each thread sort one file independentely*/
    for( i=0; i <n; ++i){
        postfix[0] =   i + 1 + '0';
        arg[i].in = strcat2(strA, postfix);
        arg[i].out = strcat2(strB, postfix);

        if(pthread_create(tid+i, NULL, sort_file, (void *)&arg[i] ) ){
            /* since all thread belongs to the same process
              exit the process cause the auto termination
              of ALL thread*/
            fprintf(stderr, "Thread creation ERROR\n");
            exit(EXIT_FAILURE);
        }


        /* no more used: free when thread join*/
        pthread_join(*(tid+i), (void *) &joined);
        if(joined){
            free(arg[i].in);
            free(arg[i].out);
        }

    }


    free(arg);


    return 0;
}




char *strcat2(const char *src, const char *postfix){
    char* tmp;

    tmp = (char*)malloc(strlen(src) + strlen(postfix) );
    tmp = strcpy(tmp, src );
    strcat(tmp, postfix );

    return tmp;

}


/*sort */
static void BottomUpMergeSort(int *A, int N){
    int i, m, l=0, r=N-1;
    int *B = (int *)malloc(N*sizeof(int));
    if (B == NULL) {
        printf("Memory allocation error\n");
        exit(1);
    }

    for (m = 1; m <= r - l; m =(m + m) ) {
        for (i = l; i <= r-m ; i +=( m + m) )
            Merge(A, B, i, i+m-1, min(i+m+m-1,r));
    }

    free(B);
}

static  int min(int A, int B) {
    if (A < B)
        return A;
    else
        return B;
}
static void Merge(int *A, int *B, int l ,int  q, int r){
    int i, j, k;
    i = l;
    j = q+1;
    for(k = l; k <= r; k++)
        if (i > q)
            B[k] = A[j++];
        else if (j > r)
            B[k] = A[i++];
        else if (    A[i] <= A[j]  )
            B[k] = A[i++];
        else
            B[k] = A[j++];
    for ( k = l; k <= r; k++ )
        A[k] = B[k];

}


/* function handle file*/
static void  *sort_file( void *arg){

    int *data = NULL, n, i;
    FILE *fin=NULL ,*fout=NULL;

    fin = fopen( ( (file_t*)arg )->in, "r");
    fout = fopen( ( (file_t*)arg )->out, "w");


    if(fin == NULL || fout == NULL){
        fprintf(stderr, "Error opening file\n");
        exit(EXIT_FAILURE);
    }
    fscanf(fin, "%d", &n);

    data = malloc(n*sizeof(n));
    for ( i = 0; i < n; i++)
        fscanf(fin, "%d\n", data+i);

    BottomUpMergeSort(data, n);

    for ( i = 0; i < n; i++)
        fprintf(fout, "%d\n", data[i]);

    /* close file*/
    fclose(fin);
    fclose(fout);
    free(data);

    pthread_exit((int*)0);
}