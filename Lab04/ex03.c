#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/wait.h>
#define N 256
#define LENPARAM 25

int main(int argc, char **argv){
    FILE *fin=NULL;
    char tmp[N];
    char delimiter[] ={' ', '\n', '\0'};
    char **token=malloc(N*sizeof(char*));

    int i=0, outcome, status;

    if(argc!=2){
        printf("Insert valid number of parameters!\n");
        return -1;
    }
    fin=fopen(argv[1], "r");

    if(fin==NULL){
        fprintf(stderr,"Problem reading file\n");
        return -2;
    }

    //we assume that all cmd inserted have parameters
    //read file and use system call system
    while( fgets(tmp, N,fin)!=NULL){
        printf("System cmd\n");
        //divide tmp string into tokens
        //fst token is cmd and other ones the params
        token[0]= strtok(tmp,"\n" );

        //sys call
        outcome = system(token[0]);
        if(outcome == -1 || outcome==127 ){
            exit(EXIT_FAILURE);
        }


    }


    rewind(fin);

    //read file and use system call exec
    while( fgets(tmp, N,fin)!=NULL){
        i=0;
        printf("Exec cmd\n");
        //divide tmp string into tokens
        //fst token is cmd and other ones the params
        token[i]= strtok(tmp, delimiter);

        while(token[i++]!=NULL){
            //usage of NULL is requested for subsequent calls
            //of the same tokenized string
            token[i] = strtok(NULL, delimiter);
        }
        //add the lst arg as NULL ptr
        // not useful because
        //sys call

        //use fork in order to execute multiple fork
        if(!fork()){
            //remember to reset the index i otherwise add i here
            if(execvp(*token,token)<0){
                exit(EXIT_FAILURE);
            }
            exit(0);
        }
        else{
            if(wait((int*)0) ==-1)
                fprintf(stderr,"Waiting error\n");
            sleep(3);
        }

        // execution of free is done in child process only if occurs errors
        // but is always executed in parent process


    }

    free(token);
    return 0;


}
